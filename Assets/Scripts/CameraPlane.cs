﻿using UnityEngine;
using System.Collections;

public class CameraPlane : MonoBehaviour {
    WebCamTexture tex = null;
	// Use this for initialization
	void Start () {
        tex = new WebCamTexture();
        gameObject.GetComponent<Renderer>().material.mainTexture = tex;
        tex.Play();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
