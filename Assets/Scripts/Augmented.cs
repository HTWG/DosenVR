﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Augmented : MonoBehaviour {
    public RawImage targetImage = null;
    WebCamTexture tex;
	// Use this for initialization
	void Start () {
	    if(targetImage != null)
        {
            tex = new WebCamTexture();
            tex.Play();
            targetImage.texture = tex;
        }
	}
	
    public void stopWebCam()
    {
        if (targetImage != null)
        {
            targetImage.enabled = false;
        }
    }

    public void startWebCam()
    {
        if (targetImage != null)
        {
            targetImage.enabled = true;
        }
    }
	// Update is called once per frame
	void Update () {
	
	}
}
