﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class KeyControl : MonoBehaviour {
    public KeyCode key;
    public string sceneName;
    public string scenePrefix = "Level";
    public GameObject scene = null;
    public Augmented augmented = null;

	// Use this for initialization
	void Start () {
	    if(sceneName != null)
        {
            SceneManager.LoadScene(sceneName, LoadSceneMode.Additive);
        }
        augmented = GameObject.FindObjectOfType<Augmented>();
	}
	
	// Update is called once per frame
	void Update () {
	    if(scene != null)
        {
            if(Input.GetKeyDown(key))
            {
                scene.SetActive(true);
                augmented.stopWebCam();
            }
            if(Input.GetKeyUp(key))
            {
                scene.SetActive(false);
                augmented.startWebCam();
            }
        }
        else
        {
            scene = GameObject.Find(sceneName + "_" + scenePrefix);
            if (scene != null)
            {
                scene.SetActive(false);
            }
        }
	}
}
