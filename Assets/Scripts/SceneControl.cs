﻿using UnityEngine;
using System.Collections;

public class SceneControl : MonoBehaviour {
    AudioSource music = null;
    // Use this for initialization
    bool started = false;
	void Start () {
        Debug.Log("Awake!");
        music = gameObject.GetComponent<AudioSource>();
	}

    void OnEnable()
    {
        if (music != null)
        {
           music.Play();
        }
    }

	// Update is called once per frame
	void Update () {
	
	}
}
